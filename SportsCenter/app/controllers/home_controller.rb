require 'json'
require 'open-uri'
require 'net/http'

class HomeController < ApplicationController
  def index
    get_weather_data
    get_news_data
    get_bookings
  end

  def get_weather_data
    # setting local longitude and latitude
    lat = "53.349804"
    long = "-6.260310"

    #url = URI.parse("https://api.darksky.net/forecast/2fdfde3c4952c3531059a4fa4d27f651/"+lat+","+long)


    url = 'https://api.darksky.net/forecast/2fdfde3c4952c3531059a4fa4d27f651/'+lat+','+long

    req = open(url)
    response_body = req.read

    @desc = JSON.parse(response_body)['currently']['summary']
    @FH = Float(JSON.parse(response_body)['currently']['temperature'])
    # every weather API I sign up returns Kelvin or fahrenheight - so decided it'll be easier to manually convert :)
    @temp = Integer((@FH - 32) * 5/9)


  end

  def get_news_data
    url = 'https://newsapi.org/v2/top-headlines?'\
      'sources=bleacher-report&'\
      'apiKey=cd743b4f5b2b4f608be92e1533874d90'
    req = open(url)
    top_news = req.read
    @no_of_results = Integer(JSON.parse(top_news)['totalResults'])
    @articles = JSON.parse(top_news)['articles']
  end

  def get_bookings
    # @bookings = Booking.where('"Full_Name" LIKE :name AND patients.user_id = :id', {:name => "%#{params[:Full_Name]}%", :id => current_user.id})
    # @bookings = Booking.where("date >= :current_date AND user = :id", {:current_date => Date.today.to_s, :id => current_user.id} )
    if current_user.admin?
      @bookings = Booking.all
    else
      @bookings = current_user.bookings.all
    end
  end

  helper_method :get_bookings
  helper_method :get_weather_data
  helper_method :get_news_data
end
