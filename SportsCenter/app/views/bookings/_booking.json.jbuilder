json.extract! booking, :id, :lot, :price, :duration, :date, :user_id, :created_at, :updated_at
json.url booking_url(booking, format: :json)
