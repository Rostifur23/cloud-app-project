Rails.application.routes.draw do

  resources :bookings
  resources :profiles
  devise_for :users
  get 'about/index'
  root 'home#index'

  get '/home', :controller=>'home', :action=>'index'
  get '/about', :controller=>'about', :action=>'index'
  get '/contact', :controller=>'contact', :action=>'index'
  get '/signedinuserprofile', :controller=>'profiles', :action=>'signedinuserprofile'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
