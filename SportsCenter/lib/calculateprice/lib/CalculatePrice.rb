require 'singleton'

class CalculatePrice
	include Singleton

	def self.calculate_price(lot, duration)

		if lot == "basketball"
		   hourlyRate = 12
		elsif lot == "football"
		   hourlyRate = 10
		elsif lot == "squash"
		   hourlyRate = 9
		elsif lot == "tennis"
		   hourlyRate = 7.5
		elsif lot == "golf"
		   hourlyRate = 10
		elsif lot == "gym"
		   hourlyRate = 6.5
		elsif lot == "swimming pool"
		   hourlyRate = 5
		end

	finalPrice = duration*hourlyRate

	return finalPrice

	end

end

