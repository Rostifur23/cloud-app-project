Gem::Specification.new do |s|
 s.name = 'calculateprice'
 s.version = '0.0.0'
 s.date = '2019-03-29'
 s.summary = "Calculates the price of a booking in a sports facility"
 s.description = "A simple gem"
 s.authors = ["rostifur23"]
 s.email = 'ross.delaney23@gmail.com'
 s.homepage = 'http://rubygems.org/gems/calculateprice'
 s.files = ["lib/CalculatePrice.rb"]
end